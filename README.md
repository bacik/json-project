**JSON Project**
Last update - **3rd September 2020** - refactored the folder structure and applied the .gitignore file properly so none of the user information files are shared.

My little project is just an illustration of my skills in Java.
The main goal of my project is to show three highest and three lowest Standard VAT rates for countries in European Union.
This is accomplished by parsing JSON from https://euvatrates.com/rates.json using Jackson library. The URL provided is a test site with JSON containing VAT rates for European Union countries.