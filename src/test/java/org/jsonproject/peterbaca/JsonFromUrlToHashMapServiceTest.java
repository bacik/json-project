package org.jsonproject.peterbaca;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.io.IOException;

public class JsonFromUrlToHashMapServiceTest {
    private JsonFromUrlToHashMapService myJsonFromUrl;

    @Before
    public void setUp () {
        myJsonFromUrl = new JsonFromUrlToHashMapService();
    }

    @Test
    public void nullInUrl () throws IOException {
        myJsonFromUrl.saveJsonFromUrlToFile(null);
        Assert.assertTrue(myJsonFromUrl.getCountryStandardRateMap().isEmpty());
    }

    @Test
    public void emptyStringInUrl () throws IOException {
        myJsonFromUrl.saveJsonFromUrlToFile("");
        Assert.assertTrue(myJsonFromUrl.getCountryStandardRateMap().isEmpty());
    }

    @Test
    public void savesFileNormally () throws IOException {
        myJsonFromUrl.saveJsonFromUrlToFile("https://euvatrates.com/rates.json");
        Assert.assertTrue(myJsonFromUrl.getMyPathToFile().exists());
    }
}